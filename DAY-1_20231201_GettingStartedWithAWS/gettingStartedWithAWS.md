#### Getting Started with AWS

#### Session Video :
    https://drive.google.com/file/d/1CPS35HQ0RSh2B1phh97xK7DGU_vpYVR7/view?usp=sharing

#### Tools:
    1. Chrome
        https://www.google.com/chrome/?brand=JJTC&gclid=EAIaIQobChMIxuDHysTtggMVpqNmAh2emwtjEAAYASAAEgL06vD_BwE&gclsrc=aw.ds

    2. Git :
        https://git-scm.com/

    3. VSCode :
        https://code.visualstudio.com/


#### Account Creation :
    1. GitLab Account Creation :
        https://about.gitlab.com/

    2. Account Creation with AWS :
        https://aws.amazon.com/free/


#### Create Account with AWS 

```
    Go to the AWS website at https://aws.amazon.com/

    STEP-1 : Click on the "Create an AWS Account" button.

    STEP-2 : Enter your email address and choose a password for your AWS account.

    STEP-3 : Enter your contact information, such as your name, company name (if applicable), and phone number.

    STEP-4 : Enter your payment information. AWS offers a free tier for new customers, but you will need to provide a credit card or debit card to verify your identity.

    STEP-5 : Choose a support plan. AWS offers different levels of support depending on your needs and budget. You can choose from Basic, Developer, Business, or Enterprise support plans.

    STEP-6 : Review the AWS Customer Agreement and check the box to confirm that you agree to the terms and conditions.

    STEP-7 : Click the "Create Account and Continue" button.

    STEP-8 : AWS will send a verification code to your email address. Enter the code in the field provided and click the "Verify Code and Continue" button.

    STEP-9 : Choose a username and enter additional information to complete your account setup. You can also select your preferred language and time zone.

    STEP-10 : You will be redirected to the AWS Management Console, where you can start using AWS services and resources.

    That's it! You have successfully created an AWS account and can now start exploring the AWS platform.


```